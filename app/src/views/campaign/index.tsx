import React, { useEffect, useState } from "react";
import { Button, Row, Col, Divider } from "antd";
import to from "await-to-js";
import CampaignService from "@/core/services/CampaignService";
import { AppTable } from "@/src/components/app-table";

export default function CampaignIndex(): JSX.Element {
  const campaignService = new CampaignService();

  const columns = [
    {
      key: "no",
      title: "#",
      width: 40,
      render: (text: string, record: any, index: number) => ++index,
    },
    {
      key: "record.name",
      title: "Campaign Name",
      dataIndex: "name",
      render: function renderName(text: string, record: any) {
        return <a href={"#/campaign/edit/" + record.id}>{text}</a>;
      },
    },
    {
      key: "record.templateId",
      title: "Template",
      dataIndex: "templateId",
    },
    {
      key: "record.status",
      title: "Status",
      dataIndex: "status",
    },
    {
      key: "record.startTime",
      title: "Start Time",
      dataIndex: "startTime",
    },
    {
      key: "record.endTime",
      title: "End Time",
      dataIndex: "endTime",
    },
  ];

  const fetchFn = async (values: any) => {
    const [err, result]: any[] = await to(campaignService.index(values));
    if (err) {
      console.log(err);
      return;
    }
    return result;
  };

  return (
    <div>
      <div className="text-right">
        <Button type="primary" href="#/campaign/create">
          Create
        </Button>
      </div>
      <Divider></Divider>
      <div>
        <AppTable columns={columns} data={[]} fetchFn={fetchFn} rowKey="id" />
      </div>
    </div>
  );
}
