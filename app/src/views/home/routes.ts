const routes: RouteConfig[] = [
  {
    key: 'Home',
    path: '/home',
    createConfig: {
      single: false,
    },
  },
]

export default routes
