import React, { useEffect, useState } from "react";
import { Button, Form, Input, Typography, Select, DatePicker, InputNumber } from "antd";
import CampaignService from "@/core/services/CampaignService";
import EmailService from "@/core/services/EmailService";
import TemplateService from "@/core/services/TemplateService";

import to from "await-to-js";
import { useHistory } from "react-router-dom";
import enUS from "antd/lib/calendar/locale/en_US";

const layout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 12 },
};

const CampaignCreate = (): JSX.Element => {
  const [loading, setLoading] = useState(false);
  const [templates, setTemplates] = useState([]);
  const [emails, setEmails] = useState([]);
  const history = useHistory();

  const campaignService = new CampaignService();
  const templateService = new TemplateService();
  const emailService = new EmailService();

  useEffect(() => {
    (async () => {
      setLoading(true);
      const getTemplates = await templateService.index({ pageSize: -1 });
      const getEmails = await emailService.index({ pageSize: -1 });
      const selectTemplates = getTemplates.data.data.map((value: any) => {
        return {
          label: value.name,
          value: value.id,
        };
      });
      const selectEmails = getEmails.data.data.map((value: any) => {
        return {
          label: value.email,
          value: value.id,
        };
      });
      setTemplates(selectTemplates);
      setEmails(selectEmails);
      setLoading(false);
    })();
  }, []);

  const handleOnFinish = async (values: any) => {
    setLoading(true);
    const [err] = await to(campaignService.post(values));
    if (err) {
      return;
    }
    setLoading(false);
    history.push("/campaign");
  };
  return (
    <div>
      <Typography.Title level={3} className="text-center">
        CampaignCreate
      </Typography.Title>
      <Form {...layout} onFinish={handleOnFinish}>
        <Form.Item
          label="Campaign Name"
          name="name"
          rules={[{ required: true, message: "Campaign Name is required!" }]}
        >
          <Input placeholder="Type a name" />
        </Form.Item>
        <Form.Item
          label="Use Email"
          name="emailId"
          rules={[{ required: true, message: "Email is required!" }]}
        >
          <Select options={emails} placeholder="Select an email to use" />
        </Form.Item>
        <Form.Item
          label="Use Template"
          name="templateId"
          rules={[{ required: true, message: "Template is required!" }]}
        >
          <Select options={templates} placeholder="Select a template to use" />
        </Form.Item>
        <Form.Item
          label="Start Time"
          name="startTime"
          rules={[{ required: true, message: "Start Time is required!" }]}
        >
          <DatePicker showTime  style={{width: "100%"}}/>
        </Form.Item>
        <Form.Item
          label="Delay Time (seconds)"
          name="delayTime"
          rules={[
            { required: true, message: "Delay Time is required!" },
            { min: 10, message: "Delay Time cannot set below 10" },
          ]}
          initialValue={10}
        >
          <InputNumber style={{width: "100%"}}/>
        </Form.Item>
        <div className="text-center">
          <Button className="mr-8" type="default" href="#/campaign">
            Back
          </Button>
          <Button type="primary" htmlType="submit" loading={loading}>
            Submit
          </Button>
        </div>
      </Form>
    </div>
  );
};

export default CampaignCreate;
