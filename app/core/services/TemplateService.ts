import BaseService from './BaseService';

class TemplateService extends BaseService {
  constructor () {
    super();
    this.name = 'templates';
  }

}

export default TemplateService