import BaseService from "./BaseService";

class UserService extends BaseService {
  constructor() {
    super();
    this.name = "users";
  }

  async login(data: AnyObj): Promise<any> {
    return await this.req(`login`, data, { method: "POST" });
  }
}

export default UserService;
