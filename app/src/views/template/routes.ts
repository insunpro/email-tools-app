const routes: RouteConfig[] = [
  {
    key: 'TemplateIndex',
    path: '/template',
    createConfig: {
      single: false,
    },
  },
  {
    key: 'TemplateCreate',
    path: '/template/create',
    createConfig: {
      single: false,
    }
  },
  {
    key: 'TemplateEdit',
    path: '/template/edit/:id',
    createConfig: {
      single: false,
    }
  },
]

export default routes
