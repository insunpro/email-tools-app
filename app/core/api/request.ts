import axios, { AxiosRequestConfig } from 'axios'
import path from 'path'
import { errorAction } from './handle-response'

// axios Cross-domain request carrying cookie
// axios.defaults.withCredentials = true
// const token = localStorage.getItem('token') || '';
// axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;

const DEFAULT_CONFIG = {
  method: 'POST',
  host: process.env.API_HOST,
  protocol: process.env.API_PROTOCOL,
  baseUrl: process.env.API_BASE_PATH,
  timeout: 30000,
  loading: false,
  errorType: 'notification',
  checkStatus: true,
  headers: {
    'Content-Type': 'application/json',
  },
}

// Parameters passed by default
const DEFAULT_PARAMS = {
  // TODO Default parameters passed in each request, This needs to be passed manually in some cases token Very useful in the scenario
}

/**
 * Initiate a request
 * @param apiPath
 * @param params
 * @param optionsSource
 */
export async function request<T extends AnyObj = AnyObj>(
  apiPath: string,
  params?: RequestParams,
  optionsSource?: RequestOptions
): Promise<T> {
  const options: RequestOptions = Object.assign({}, DEFAULT_CONFIG, optionsSource)
  const { method, protocol, host, baseUrl, headers, responseType, checkStatus, formData } = options
  const sendData: AxiosRequestConfig = {
    url: `${protocol}${path.join(host || '', baseUrl || '', apiPath || '')}`,
    method,
    headers,
    responseType,
  }

  const paramsData = Object.assign({}, DEFAULT_PARAMS, params)

  if (method === 'GET') {
    sendData.params = params
  } else if (formData) {
    const formData = new FormData()
    Object.keys(paramsData).forEach((key) => {
      formData.append(key, paramsData[key])
    })
    sendData.data = formData
  } else {
    sendData.data = paramsData
  }
  return axios(sendData)
    .then((res) => {
      const data: T = res.data

      // TODO Set success conditions according to the back-end interface, For example here `data.code == 200`
      if (!checkStatus || data.code == 200) {
        return data
      } else {
        return Promise.reject(data)
      }
    })
    .catch(async (err) => {
      await errorAction(err, sendData, options)
      return Promise.reject({ ...err, path: apiPath, sendData, resData: err })
    })
}

/** - interface - split ------------------------------------------------------------------- */

declare global {
  /**
   * Network request parameters
   */
  interface RequestParams {
    [key: string]: any
  }

  /**
   * Network request return value
   */
  interface RequestRes {
    // TODO Various return value formats emerge endlessly, Please redefine the type according to the actual content
    /** status code,Successfully returned 200 */
    code: number
    /** wrong information */
    message: string
    /** Return data */
    data: any
  }

  /**
   * Request options
   */
  interface RequestOptions {
    /** Request type: [POST | GET] default: POST */
    method?: 'GET' | 'DELETE' | 'HEAD' | 'OPTIONS' | 'POST' | 'PUT' | 'PATCH'
    /** Basic url, No special needs, no need to deliver */
    baseUrl?: string
    /** Request domain name */
    host?: string
    /** protocol */
    protocol?: string
    /** use formData Pass parameters */
    formData?: boolean
    /** Interface grouping */
    group?: string
    /** overtime time,unit: ms */
    timeout?: number
    /** Whether to display during the request Loading */
    loading?: boolean
    /** The prompt box type when an error occurs, default: notification */
    errorType?: 'notification' | 'modal' | false
    /** Custom request header */
    headers?: any
    /** Type dynamic setting */
    responseType?: 'arraybuffer' | 'blob' | 'document' | 'json' | 'text' | 'stream'
    /** Whether to verify request status */
    checkStatus?: boolean
  }
}
