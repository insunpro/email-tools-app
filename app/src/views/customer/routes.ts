const routes: RouteConfig[] = [
  {
    key: 'CustomerIndex',
    path: '/customer',
    createConfig: {
      single: false,
    },
  },
  {
    key: 'CustomerCreate',
    path: '/customer/create',
    createConfig: {
      single: false,
    }
  },
  {
    key: 'CustomerEdit',
    path: '/customer/edit/:id',
    createConfig: {
      single: false,
    }
  },
  {
    key: 'CustomerImport',
    path: '/customer/import',
    createConfig: {
      single: false,
    }
  },
]

export default routes
