const routes: RouteConfig[] = [
  {
    key: 'EmailIndex',
    path: '/email',
    createConfig: {
      single: false,
    }
  },
  {
    key: 'EmailCreate',
    path: '/email/create',
    createConfig: {
      single: false,
    }
  },
  {
    key: 'EmailEdit',
    path: '/email/edit/:id',
    createConfig: {
      single: false,
    }
  },
]

export default routes
