import React, { useEffect, useState } from "react";
import { Button, Row, Col, Divider } from "antd";
import to from "await-to-js";
import CustomerService from "@/core/services/CustomerService";
import { AppTable } from "@/src/components/app-table";

export default function CustomerIndex(): JSX.Element {
  const customerService = new CustomerService();

  const columns = [
    {
      key: "no",
      title: "#",
      width: 40,
      render: (text: string, record: any, index: number) => ++index,
    },
    {
      key: "record.email",
      title: "Email",
      dataIndex: "email",
      render: function renderEmail(text: string, record: any) {
        return <a href={"#/customer/edit/" + record.id}>{text}</a>;
      },
    },
    {
      key: "record.firstName",
      title: "First Name",
      dataIndex: "firstName",
    },
    {
      key: "record.lastName",
      title: "Last Name",
      dataIndex: "lastName",
    },
    {
      key: "record.phone",
      title: "Phone",
      dataIndex: "phone",
    },
    {
      key: "record.address",
      title: "Address",
      dataIndex: "address",
    },
  ];

  const fetchFn = async (values: any) => {
    return await customerService.index(values);
  };

  return (
    <div>
      <div className="text-right">
        <Button type="default" href="#/customer/import">
          Import
        </Button>
        <Button type="primary" href="#/customer/create">
          Create
        </Button>
      </div>
      <Divider></Divider>
      <div>
        <AppTable columns={columns} fetchFn={fetchFn} rowKey="id" />
      </div>
    </div>
  );
}
