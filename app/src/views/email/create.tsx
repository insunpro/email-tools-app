import React, { useEffect, useState } from "react";
import { Button, Form, Input, Col, Row, Typography, Select, Radio } from "antd";
import EmailService from "@/core/services/EmailService";
import to from "await-to-js";
import { useHistory } from "react-router-dom";

const layout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 12 },
};

const EmailCreate = (): JSX.Element => {
  const [loading, setLoading] = useState(false);
  const [type, setType] = useState("smtp");
  const emailService = new EmailService();
  const history = useHistory();
  const emailTypes = EmailService.getTypes();

  const handleOnFinish = async (values: any) => {
    setLoading(true);
    const [err, result]: any[] = await to(emailService.post(values));
    if (err) {
      return;
    }
    setLoading(false);
    history.push("/email");
  };
  return (
    <div>
      <Typography.Title level={3} className="text-center">
        EmailCreate
      </Typography.Title>
      <Form
        {...layout}
        onFinish={handleOnFinish}
        initialValues={{
          email: "",
          type: "smtp",
        }}
      >
        <Form.Item
          label="Email"
          name="email"
          rules={[{ required: true, message: "Email is required!" }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Services"
          name="type"
          rules={[{ required: true, message: "Email Services is required!" }]}
        >
          <Radio.Group onChange={(e) => setType(e.target.value)}>
            {emailTypes.map((value) => {
              return (
                <Radio key={value.value} value={value.value}>
                  {value.label}
                </Radio>
              );
            })}
          </Radio.Group>
        </Form.Item>
        {type === "smtp" && (
          <>
            <Form.Item
              label="SMTP Host"
              name={["smtp", "host"]}
              rules={[{ required: true, message: "SMTP Host is required!" }]}
              initialValue={"smtp.gmail.com"}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label="SMTP Port"
              name={["smtp", "port"]}
              rules={[{ required: true, message: "SMTP Port is required!" }]}
              initialValue={465}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label="SMTP Password"
              name={["smtp", "password"]}
              rules={[
                { required: true, message: "SMTP Password is required!" },
              ]}
            >
              <Input />
            </Form.Item>
          </>
        )}
        {type === "sendGrid" && (
          <>
            <Form.Item
              label="Secret Key"
              name={["sendGrid", "secret"]}
              rules={[{ required: true, message: "Secret Key is required!" }]}
            >
              <Input />
            </Form.Item>
          </>
        )}

        <div className="text-center">
          <Button className="mr-8" type="default" href="#/email">
            Back
          </Button>
          <Button type="primary" htmlType="submit" loading={loading}>
            Submit
          </Button>
        </div>
      </Form>
    </div>
  );
};

export default EmailCreate;
