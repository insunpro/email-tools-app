import React from 'react'
import { Button, Result } from 'antd'

import './index.less'

const Error500 = (): JSX.Element => {
  return (
    <div className="layout-padding flex column center no-match">
      <Result
        status="500"
        title="500"
        subTitle={<p className="text-default">Sorry, there seem no connection to server. Please check your ethernet connection or contact to supporter!</p>}
        extra={
          <Button type="primary" onClick={() => history.go(-1)}>
            Back
          </Button>
        }
      />
    </div>
  )
}

export default Error500