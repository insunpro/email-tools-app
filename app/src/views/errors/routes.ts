const routes: RouteConfig[] = [
  {
    key: 'Error',
    path: '/errors',
    windowOptions: {
      title: 'Error: Not Found',
    },
  },
  {
    key: 'Error404',
    path: '/errors/404',
    windowOptions: {
      title: 'Error: Not Found',
    },
  },
  {
    key: 'Error500',
    path: '/errors/500',
    windowOptions: {
      title: 'Error: Connection Fail',
    },
  },
]

export default routes
