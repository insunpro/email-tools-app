import React, { useEffect, useState } from "react";
import { Button, Form, Input, Col, Row, Typography, Skeleton } from "antd";
import CustomerService from "@/core/services/CustomerService";
import to from "await-to-js";
import { useHistory } from "react-router-dom";

const layout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 12 },
};

const CustomerEdit = (props: PageProps): JSX.Element => {
  const [loading, setLoading] = useState(false);
  const [fetching, setFetching] = useState(true);
  const customerService = new CustomerService();
  const history = useHistory();
  const [form] = Form.useForm();
  const id = props.match.params.id;

  const fetchFn = async () => {
    const [err, result]: any[] = await to(customerService.detail({ id }));
    if (err) {
      return;
    }
    form.setFieldsValue(result.data);
    setFetching(false);
  };

  useEffect(() => {
    fetchFn();
  }, []);

  const handleOnFinish = async (values: any) => {
    setLoading(true);
    const [err] = await to(customerService.edit({ ...values, id }));
    if (err) {
      return;
    }
    setLoading(false);
    history.push("/customer");
  };
  return (
    <div>
      <Typography.Title level={3} className="text-center">
        CustomerEdit
      </Typography.Title>

      <Form {...layout} form={form} onFinish={handleOnFinish}>
        <Form.Item
          label="Email"
          name="email"
          rules={[{ required: true, message: "Please input email!" }]}
        >
          {fetching ? (
            <Skeleton.Input active style={{ width: 600 }} />
          ) : (
            <Input />
          )}
        </Form.Item>
        <Form.Item label="First Name" name="firstName">
          {fetching ? (
            <Skeleton.Input active style={{ width: 600 }} />
          ) : (
            <Input />
          )}
        </Form.Item>
        <Form.Item label="Last Name" name="lastName">
          {fetching ? (
            <Skeleton.Input active style={{ width: 600 }} />
          ) : (
            <Input />
          )}
        </Form.Item>
        <Form.Item label="Phone" name="phone">
          {fetching ? (
            <Skeleton.Input active style={{ width: 600 }} />
          ) : (
            <Input />
          )}
        </Form.Item>
        <Form.Item label="Address" name="address">
          {fetching ? (
            <Skeleton.Input active style={{ width: 600 }} />
          ) : (
            <Input />
          )}
        </Form.Item>
        <div className="text-center">
          <Button className="mr-8" type="default" href="#/customer">
            Back
          </Button>
          <Button
            type="primary"
            htmlType="submit"
            loading={loading || fetching}
          >
            Submit
          </Button>
        </div>
      </Form>
    </div>
  );
};

export default CustomerEdit;
