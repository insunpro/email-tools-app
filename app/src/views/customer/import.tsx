import React, { useEffect, useState } from "react";
import { Button, Row, Col, Divider, Upload, Table } from "antd";
import to from "await-to-js";
import CustomerService from "@/core/services/CustomerService";
import { RcFile } from "antd/lib/upload";
import { UploadOutlined } from "@ant-design/icons";
import XlsxUtil from "@/core/utils/Xlsx";
import { useHistory } from "react-router-dom";

export default function CustomerImport(): JSX.Element {
  const [fileList, setFileList]: any[] = React.useState([]);
  const [data, setData]: any[] = React.useState([]);
  const [loading, setLoading] = React.useState(false);

  const customerService = new CustomerService();
  const history = useHistory();

  const columns = [
    {
      key: "record.row",
      title: "No.",
      dataIndex: "row",
    },
    {
      key: "record.email",
      title: "Email",
      dataIndex: "email",
      render: function renderEmail(text: string, record: any) {
        return <a href={"#/customer/edit/" + record.id}>{text}</a>;
      },
    },
    {
      key: "record.firstName",
      title: "First Name",
      dataIndex: "firstName",
    },
    {
      key: "record.lastName",
      title: "Last Name",
      dataIndex: "lastName",
    },
    {
      key: "record.phone",
      title: "Phone",
      dataIndex: "phone",
    },
    {
      key: "record.address",
      title: "Address",
      dataIndex: "address",
    },
  ];

  const handleBeforeUpload = (file: RcFile, files: RcFile[]) => {
    setFileList(files);
    handleFile(file);
    return false;
  };

  const handleFile = async (file: RcFile) => {
    const uploadData: any[] = await XlsxUtil.readFileFromInput(file);
    const titles: string[] = uploadData[0];

    uploadData.shift();
    const convertData: any[] = [];
    uploadData.map((arr: string[], index) => {
      if (!arr[getIndex("custom_order_number", titles)]) return;
      convertData.push({
        row: Number(index) + 1,
        email: getEmail(
          arr[getIndex("first_name", titles)],
          arr[getIndex("last_name", titles)]
        ),
        firstName: arr[getIndex("first_name", titles)],
        lastName: arr[getIndex("last_name", titles)],
        address: arr[getIndex("address", titles)],
        phone: arr[getIndex("phone", titles)],
      });
    });
    console.log("convertData", convertData.length);
    setData(convertData);
  };

  const getIndex = (field: string, array: string[]) => {
    return array.findIndex(
      (value) =>
        value && value.replaceAll(" ", "_").toLowerCase().indexOf(field) != -1
    );
  };

  const getEmail = (firstName: string, lastName: string) => {
    return (
      firstName.replaceAll(" ", "").toLowerCase() +
      "." +
      lastName.replaceAll(" ", "").toLowerCase() +
      "@gmail.com"
    );
  };

  const handleSubmit = async () => {
    setLoading(true);
    const [err, result]: any[] = await to(customerService.importCustomer(data));
    if (err) {
      console.log(err);
      setLoading(false);
      return;
    }
    console.log(result);
    history.push("/customer");
  };

  return (
    <div>
      <div>
        <Upload
          multiple={false}
          fileList={fileList}
          beforeUpload={handleBeforeUpload}
          showUploadList={false}
        >
          <Button
            icon={<UploadOutlined />}
            loading={loading}
            className="btn-margin-right"
            type="primary"
          >
            Upload
          </Button>
        </Upload>
      </div>
      <Divider></Divider>
      <div>
        <Table
          dataSource={data}
          columns={columns}
          rowKey="row"
          size="small"
          pagination={{
            defaultPageSize: 10,
          }}
          footer={(pageData) => {
            return data.length ? (
              <p style={{ textAlign: "center" }}>Total: {data.length}</p>
            ) : (
              ""
            );
          }}
        ></Table>
      </div>
      <Divider></Divider>
      <div className="text-center">
        <Button className="mr-8" type="default" href="#/customer">
          Back
        </Button>
        <Button
          type="primary"
          disabled={!data || !data.length}
          onClick={handleSubmit}
          loading={loading}
        >
          Submit
        </Button>
      </div>
    </div>
  );
}
