interface TemplateModel {
  name: string;
  fileSize: string;
  thumbnail: string;
}