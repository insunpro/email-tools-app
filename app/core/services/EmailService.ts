import BaseService from './BaseService';

class EmailService extends BaseService {
  constructor () {
    super();
    this.name = 'emails';
  }

  static getTypes(): EmailType[] {
    return [
      {
        label: "Smtp Server",
        value: "smtp"
      },
      {
        label: "Send Grid",
        value: "sendGrid"
      },
      {
        label: "Amazon SES",
        value: "ses"
      }
    ]
  }
}

export default EmailService

interface EmailType {
  label: string;
  value: "smtp" | "sendGrid" | "ses"
}