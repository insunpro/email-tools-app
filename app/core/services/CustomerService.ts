import BaseService from "./BaseService";

class CustomerService extends BaseService {
  constructor() {
    super();
    this.name = "customers";
  }

  async importCustomer(data: CustomerModel[]): Promise<any> {
    return await this.req(
      `${this.name}/import`,
      { data },
      { method: "POST", headers: this.headers }
    );
  }
}

export default CustomerService;
