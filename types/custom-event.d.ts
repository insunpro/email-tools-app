interface CustomEventMap {
  /** [Window] Custom event: route refresh */
  'router-update': CustomEvent<PageProps>
}

declare namespace Electron {
  interface WebContents {
    /** [BrowserWindow] Custom event: DOM Ready */
    send(channel: 'dom-ready', createConfig: CreateConfig): void
  }
}
