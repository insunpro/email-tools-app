import React, { useEffect, useState } from "react";
import { Button, Form, Input, Col, Row, Typography, Skeleton } from "antd";
import TemplateService from "@/core/services/TemplateService";
import to from "await-to-js";
import { useHistory } from "react-router-dom";

const layout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 12 },
};

export default function TemplateEdit(props: PageProps): JSX.Element {
  const [loading, setLoading] = useState(false);
  const [fetching, setFetching] = useState(true);
  const templateService = new TemplateService();
  const history = useHistory();
  const [form] = Form.useForm();
  const id = props.match.params.id;


  const fetchFn = async () => {
    const result: RequestResponse<TemplateModel> = await templateService.detail({id})
    form.setFieldsValue(result.data)
    setFetching(false)
  }


  useEffect(() => {
    fetchFn()
  }, [])


  const handleOnFinish = async (values: AnyObj): Promise<void> => {
    setLoading(true);
    const [err] = await to(templateService.edit({...values, id}));
    if (err) {
      return
    }
    setLoading(false);
    history.push("/template")
  }
  return (
    <div>
      <Typography.Title level={3} className="text-center">
        TemplateEdit
      </Typography.Title>
      
      <Form {...layout} form={form} onFinish={handleOnFinish}>
        <Form.Item label="Email" name="template" 
          rules={[{ required: true, message: 'Please input template!' }]}
        >
          {fetching ? <Skeleton.Input active style={{width: 600}}/> : <Input />}
        </Form.Item>
        <Form.Item label="SMTP Password" name="smtpPassword">
          {fetching ? <Skeleton.Input active style={{width: 600}}/> : <Input />}
        </Form.Item>
        <Form.Item label="SMTP Port" name="smtpPort">
          {fetching ? <Skeleton.Input active style={{width: 600}}/> : <Input />}
        </Form.Item>
        <Form.Item label="SendGrid Key" name="sendGridKey">
          {fetching ? <Skeleton.Input active style={{width: 600}}/> : <Input />}
        </Form.Item>
        <div className="text-center">
          <Button className="mr-8" type="default" href="#/template">
            Back
          </Button>
          <Button type="primary" htmlType="submit" loading={loading || fetching}>Submit</Button>
        </div>
      </Form>
    </div>
  );
}

