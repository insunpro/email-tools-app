import React, { useEffect, useState } from "react";
import { Button, Form, Input, Col, Row, Typography, Skeleton } from "antd";
import CampaignService from "@/core/services/CampaignService";
import to from "await-to-js";
import { useHistory } from "react-router-dom";

const sender: any = null;

const layout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 12 },
};

const tailLayout = {
  wrapperCol: { offset: 6, span: 12 },
};
const CampaignEdit = (props: PageProps): JSX.Element => {
  const [loading, setLoading] = useState(false);
  const [fetching, setFetching] = useState(true);
  const campaignService = new CampaignService();
  const history = useHistory();
  const [form] = Form.useForm();
  const id = props.match.params.id;

  const fetchFn = async () => {
    const [err, result]: any[] = await to(campaignService.detail({ id }));
    if (err) {
      return;
    }
    form.setFieldsValue(result.data);
    setFetching(false);
  };

  useEffect(() => {
    fetchFn();
  }, []);

  const handleOnFinish = async (values: any) => {
    setLoading(true);
    const [err] = await to(campaignService.edit({ ...values, id }));
    if (err) {
      return;
    }
    setLoading(false);
    history.push("/capmaign");
  };
  return (
    <div>
      <Typography.Title level={3} className="text-center">
        CampaignEdit
      </Typography.Title>

      <Form {...layout} form={form} onFinish={handleOnFinish}>
        <Form.Item
          label="Campaign Name"
          name="name"
          rules={[{ required: true, message: "Please input name!" }]}
        >
          {fetching ? (
            <Skeleton.Input active style={{ width: 600 }} />
          ) : (
            <Input />
          )}
        </Form.Item>
        <Form.Item label="Template" name="templateId">
          {fetching ? (
            <Skeleton.Input active style={{ width: 600 }} />
          ) : (
            <Input />
          )}
        </Form.Item>
        <Form.Item label="Start Time" name="startTime">
          {fetching ? (
            <Skeleton.Input active style={{ width: 600 }} />
          ) : (
            <Input />
          )}
        </Form.Item>
        <div className="text-center">
          <Button className="mr-8" type="default" href="#/capmaign">
            Back
          </Button>
          <Button
            type="primary"
            htmlType="submit"
            loading={loading || fetching}
          >
            Submit
          </Button>
        </div>
      </Form>
    </div>
  );
};

export default CampaignEdit;
