import React, { useEffect, useState } from "react";
import { Button, Form, Input, Col, Row, Typography } from "antd";
import TemplateService from "@/core/services/TemplateService";
import to from "await-to-js";
import { useHistory } from "react-router-dom";

const layout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 12 },
};

const TemplateCreate = (): JSX.Element => {
  const [loading, setLoading] = useState(false);
  const templateService = new TemplateService();
  const history = useHistory();

  const handleOnFinish = async (values: AnyObj) => {
    setLoading(true);
    const [err] = await to(templateService.post(values));
    if (err) {
      return;
    }
    setLoading(false);
    history.push("/template");
  };
  return (
    <div>
      <Typography.Title level={3} className="text-center">
        TemplateCreate
      </Typography.Title>
      <Form {...layout} onFinish={handleOnFinish}>
        <Form.Item
          label="Email"
          name="template"
          rules={[{ required: true, message: "Please input template!" }]}
        >
          <Input />
        </Form.Item>
        <Form.Item label="SMTP Password" name="smtpPassword">
          <Input />
        </Form.Item>
        <Form.Item label="SMTP Port" name="smtpPort">
          <Input />
        </Form.Item>
        <Form.Item label="SendGrid Key" name="sendGridKey">
          <Input />
        </Form.Item>
        <div className="text-center">
          <Button className="mr-8" type="default" href="#/template">
            Back
          </Button>
          <Button type="primary" htmlType="submit" loading={loading}>
            Submit
          </Button>
        </div>
      </Form>
    </div>
  );
};

export default TemplateCreate;
