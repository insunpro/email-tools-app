/**
 * Page resource collection, please do not reference in the main process
 */

// Set to undefined Will not create a route, generally used for redirection
export const Home = import('./views/home/index')

//SAMPLES
export const LogViewer = import('./views/log-viewer/log-viewer')
export const About = import('./views/about/about')

export const Error = import('./views/errors')
export const Error404 = import('./views/errors/404')
export const Error500 = import('./views/errors/500')

//CUSTOM
export const CampaignIndex = import('./views/campaign/index')
export const CampaignCreate = import('./views/campaign/create')
export const CampaignEdit = import('./views/campaign/edit')


export const CustomerIndex = import('./views/customer/index')
export const CustomerCreate = import('./views/customer/create')
export const CustomerEdit = import('./views/customer/edit')
export const CustomerImport = import('./views/customer/import')

export const EmailIndex = import('./views/email/index')
export const EmailCreate = import('./views/email/create')
export const EmailEdit = import('./views/email/edit')

export const TemplateIndex = import('./views/template/index')
export const TemplateCreate = import('./views/template/create')
export const TemplateEdit = import('./views/template/edit')

// Synchronous reference, note that this will not trigger beforeRouter
export { default as AlertModal } from './views/modals/alert-modal'
