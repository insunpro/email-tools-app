import XLSX from 'xlsx';
import _ from 'lodash';

class XlsxUtil {
  static readFileFromInput = async (file: File): Promise<any> => {
    return new Promise((resolve, reject) => {
      const reader = new FileReader()
      reader.onload = () => {
        const fileAsBinaryString = reader.result
        const workbook = XLSX.read(fileAsBinaryString, { type: 'binary' })
        const first_worksheet = workbook.Sheets[workbook.SheetNames[0]]
        const data = XLSX.utils.sheet_to_json(first_worksheet, { header: 1, raw: false })
        resolve(data)
      }
      reader.onabort = () => {
        reject()
        console.log('file reading was aborted')
      }
      reader.onerror = () => {
        reject()
        console.log('file reading has failed')
      }

      reader.readAsBinaryString(file)
    })
  }

}

export default XlsxUtil