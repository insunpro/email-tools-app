import { Notification, BrowserWindow, remote } from 'electron'

/**
 * Handling when an error occurs in a network request
 * Note that this function runs in the main process, please do not use Document API
 * @param err
 * @param sendData
 * @param options
 */
export async function errorAction(err: AnyObj, sendData: AnyObj, options: RequestOptions): Promise<void> {
  if (err.response) {
    // The request was made and the server responded with a status code
    // that falls out of the range of 2xx
    switch (err.response.status) {
      case 401:
        await $tools.createWindow('AlertModal', {
          windowOptions: {
            modal: true,
            parent: remote.BrowserWindow.getFocusedWindow() || undefined,
            title: "Required Login",
          },
          createConfig: {
            delayToShow: 100, // DESC Avoid flickering in borderless windows
          },
          query: {
            type: 'error',
            title: "Required Login",
            message: "You need login to continue!",
          },
        })
        localStorage.removeItem("token");
        localStorage.removeItem("user");
        window.location.replace("/home")
        break
      case 403:
        // window.location.replace("/errors/403")
        break
      case 404:
        // window.location.replace("/errors/404")
        break
      default:
        console.log("err.response", err.response.data);
        console.log("err.response", err.response.status);
        console.log("err.response", err.response.headers);
    }

  } else if (err.request) {
    // The request was made but no response was received
    // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
    // http.ClientRequest in node.js
    await $tools.createWindow('AlertModal', {
      windowOptions: {
        modal: true,
        parent: remote.BrowserWindow.getFocusedWindow() || undefined,
        title: "Lost Connection",
      },
      createConfig: {
        delayToShow: 100, // DESC Avoid flickering in borderless windows
      },
      query: {
        type: 'error',
        title: "Lost Connection",
        message: err.message,
      },
    })
    window.location.replace("/home")
  } else {
    // Something happened in setting up the request that triggered an Error
    console.log('Error', err.message);
  }
  console.log("err.config", err.config);

  const { code, message } = err
  const { errorType } = options

  $tools.log.error(`[request:${code}] [${errorType}]`, err)

  // switch (code) {
  //   // Jump to the unlogged page
  //   case 30000:
  //     // ...
  //     break

  //   // Jump without permission
  //   case 30002:
  //     // ...
  //     break

  //   default:
  //     const title = `Request Error: [${code}]`
  //     if (errorType === 'notification') {
  //       const n = new remote.Notification({
  //         icon: $tools.APP_ICON,
  //         title,
  //         body: message,
  //       })
  //       n.show()
  //     } else {
  //       await $tools.createWindow('AlertModal', {
  //         windowOptions: {
  //           modal: true,
  //           parent: BrowserWindow.getFocusedWindow() || undefined,
  //           title,
  //         },
  //         createConfig: {
  //           delayToShow: 100, // DESC Avoid flickering in borderless windows
  //         },
  //         query: {
  //           type: 'error',
  //           title,
  //           message,
  //         },
  //       })
  //     }
  //     break
  // }
}
