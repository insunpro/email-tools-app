import React, { useState, useEffect } from "react";
import { Table } from "antd";

interface Pagination {
  current: number;
  pageSize: number;
  total: number;
}

const AppTable = (props: AnyObj): JSX.Element => {
  const { columns, fetchFn, pagination = {}, ...otherProps } = props;

  const [loading, setLoading] = useState(false);
  const [data, setData] = useState([]);
  const [pageData, setPageData] = useState({
    total: 0,
    pageSize: 10,
    current: 1,
  });

  const reloadData = async (params: any) => {
    setLoading(true);
    if (fetchFn && typeof fetchFn === "function") {
      const result = await fetchFn({
        pageSize: params.pagination.pageSize,
        page: params.pagination.current - 1,
      });
      if (result && result.code === 200) {
        setPageData({ ...params.pagination, total: result.data.total });
        setData(result.data.data);
        setLoading(false);
      }
    }
  };

  const handleTableChange = (
    pagination: AnyObj,
  ): void => {
    reloadData({ pagination });
  };

  useEffect(() => {
    reloadData({ pagination: pageData });
  }, []);

  return (
    <div>
      <Table
        columns={columns}
        dataSource={data}
        loading={loading}
        size="small"
        bordered
        onChange={handleTableChange}
        pagination={pageData}
        {...otherProps}
      />
    </div>
  );
};

export default AppTable;
