const routes: RouteConfig[] = [
  {
    key: 'CampaignIndex',
    path: '/campaign',
    createConfig: {
      single: false,
    },
  },
  {
    key: 'CampaignCreate',
    path: '/campaign/create',
    createConfig: {
      single: false,
    },
  },
  {
    key: 'CampaignEdit',
    path: '/campaign/edit/:id',
    createConfig: {
      single: false,
    },
  },
]

export default routes
