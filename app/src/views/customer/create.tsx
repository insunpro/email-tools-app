import React, { useEffect, useState } from "react";
import { Button, Form, Input, Col, Row, Typography } from "antd";
import CustomerService from "@/core/services/CustomerService";
import to from "await-to-js";
import { useHistory } from "react-router-dom";

const layout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 12 },
};

const CustomerCreate = (): JSX.Element => {
  const [loading, setLoading] = useState(false);
  const customerService = new CustomerService();
  const history = useHistory();

  const handleOnFinish = async (values: any) => {
    setLoading(true);
    const [err]: any[] = await to(customerService.post(values));
    if (err) {
      setLoading(false);
      return;
    }
    history.push("/customer")
  }
  return (
    <div>
      <Typography.Title level={3} className="text-center">
        CustomerCreate
      </Typography.Title>
      <Form {...layout} onFinish={handleOnFinish}>
        <Form.Item label="Email" name="email" 
          rules={[{ required: true, message: 'Please input email!' }]}
        >
          <Input />
        </Form.Item>
        <Form.Item label="First Name" name="firstName">
          <Input />
        </Form.Item>
        <Form.Item label="Last Name" name="lastName">
          <Input />
        </Form.Item>
        <Form.Item label="Phone" name="phone">
          <Input />
        </Form.Item>
        <Form.Item label="Address" name="address">
          <Input />
        </Form.Item>
        <div className="text-center">
          <Button className="mr-8" type="default" href="#/customer">
            Back
          </Button>
          <Button type="primary" htmlType="submit" loading={loading}>Submit</Button>
        </div>
      </Form>
    </div>
  );
};

export default CustomerCreate;
