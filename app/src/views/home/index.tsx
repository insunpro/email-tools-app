import React, { useEffect, useState } from "react";
import { shell } from "electron";
import { Form, Input, Button, Row, Col } from "antd";
import UserService from "@/core/services/UserService";
import to from "await-to-js";
import "./home.less";

const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
};
const tailLayout = {
  wrapperCol: { offset: 8, span: 16 },
};

const HomeIndex = (): JSX.Element => {
  const [user, setUser]: any[] = useState(null);
  const userService = new UserService();

  useEffect(() => {
    const checkUser = localStorage.getItem("user");
    if (checkUser) setUser(JSON.parse(checkUser));
  }, []);

  const handleOnFinish = async (values: any) => {
    const [err, result]: any[] = await to(userService.login(values));
    if (err) {
      console.log(err);
      return;
    }
    localStorage.setItem("token", result.data.token);
    localStorage.setItem("user", JSON.stringify(result.data.user));
    setUser(result.data.user);
    return;
  };

  const handleLogout = () => {
    localStorage.removeItem("token");
    localStorage.removeItem("user");
    setUser(null);
  };
  return (
    <div>
      <Row>
        <Col span="24" className="text-center">
          <img src={$tools.APP_ICON} width="88" />
        </Col>
      </Row>
      <br />
      {user ? (
        <Row>
          <Col span="24" className="text-center">
            <p>Hello, {user.username}</p>
            <Button type="default" onClick={handleLogout}>
              Logout
            </Button>
          </Col>
        </Row>
      ) : (
        <Row>
          <Col sm={{ span: 24 }} md={{ offset: 6, span: 12 }}>
            <Form {...layout} onFinish={handleOnFinish}>
              <Form.Item
                label="Username"
                name="username"
                rules={[{ required: true, message: "Please input username!" }]}
              >
                <Input />
              </Form.Item>
              <Form.Item
                label="Password"
                name="password"
                rules={[{ required: true, message: "Please input password!" }]}
              >
                <Input.Password />
              </Form.Item>
              <Form.Item {...tailLayout}>
                <Button type="primary" htmlType="submit">
                  Login
                </Button>
              </Form.Item>
            </Form>
          </Col>
        </Row>
      )}
      <Row className="home-footer">
        <Col span="24" className="text-center">
          <p className="fs-16 text-gray">
            {$tools.APP_NAME} version {$tools.APP_VERSION}
          </p>
          <p className="fs-16 text-gray">
            Copyright © {new Date().getFullYear()}{" "}
            <a
              onClick={() => {
                shell.openExternal("https://baochill.com");
              }}
            >
              baochill.
            </a>{" "}
            All rights (demo)
          </p>
        </Col>
      </Row>
    </div>
  );
};

export default HomeIndex;
