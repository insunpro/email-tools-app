import React from "react";
import { Button, Divider } from "antd";
import TemplateService from "@/core/services/TemplateService";
import { AppTable } from "@/src/components/app-table";

export default function TemplateIndex(): JSX.Element {
  const templateService = new TemplateService();

  const columns = [
    {
      key: "no",
      title: "#",
      width: 40,
      render: (text: string, record: any, index: number) => ++index,
    },
    {
      key: "name",
      title: "Name",
      dataIndex: "name",
      render: function renderName(text: string, record: AnyObj) {
        return <a href={"#/template/edit/" + record.id}>{text}</a>;
      },
    },
    {
      key: "thumbnail",
      title: "Preview",
      dataIndex: "thumbnail",
    },
    {
      key: "fileSize",
      title: "Size",
      dataIndex: "fileSize",
    },
    {
      key: "download",
      title: "Download",
    },
  ];

  const fetchFn = async (values: AnyObj) => {
    return await templateService.index(values);
  };

  return (
    <div>
      <div className="text-right">
        {/* <Button type="primary" href="#/template/create">
          Create
        </Button> */}
      </div>
      <Divider></Divider>
      <div>
        <AppTable columns={columns} fetchFn={fetchFn} rowKey="id" />
      </div>
    </div>
  );
}
