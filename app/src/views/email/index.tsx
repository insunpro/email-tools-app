import React, { useEffect, useState } from "react";
import { Button, Row, Col, Divider } from "antd";
import to from "await-to-js";
import EmailService from "@/core/services/EmailService";
import { AppTable } from "@/src/components/app-table";

export default function EmailIndex(): JSX.Element {
  const emailService = new EmailService();
  const emailTypes = EmailService.getTypes();

  const columns = [
    {
      key: "no",
      title: "#",
      width: 40,
      render: (text: string, record: any, index: number) => ++index,
    },
    {
      key: "email",
      title: "Email",
      dataIndex: "email",
      render: function renderEmail(text: string, record: any) {
        return <a href={"#/email/edit/" + record.id}>{text}</a>;
      },
    },
    {
      key: "type",
      title: "Service",
      dataIndex: "type",
      render: function renderEmail(text: string, record: any) {
        const type = emailTypes.find(value => value.value === text)
        return type ? type.label : text;
      },
    },
  ];

  const fetchFn = async (values: AnyObj) => {
    const result = await emailService.index(values);
    return result;
  };

  return (
    <div>
      <div className="text-right">
        <Button type="primary" href="#/email/create">
          Create
        </Button>
      </div>
      <Divider></Divider>
      <div>
        <AppTable columns={columns} data={[]} fetchFn={fetchFn} rowKey="id" />
      </div>
    </div>
  );
}
