import { request } from '@/core/api/request';

class BaseApi {
  name: string;
  token: string;
  req: typeof request;
  headers: any;
  constructor () {
    this.name = "";
    this.req = request;
    this.token = String(localStorage.getItem('token') || '') 
    this.headers = {
      'Authorization': `Bearer ${this.token}`
    }
  }

  async index(data: AnyObj): Promise<any> {
    return await this.req(`${this.name}`, data, { method: 'GET', headers: this.headers })
  }

  async detail(data: AnyObj): Promise<any> {
    return await this.req(`${this.name}/${data.id}`, data, { method: 'GET', headers: this.headers })
  }

  async post(data: AnyObj, config={}): Promise<any> {
    return await this.req(`${this.name}`, data, { method: 'POST', headers: this.headers })
  }

  async edit(data: AnyObj, config={}): Promise<any> {
    return await this.req(`${this.name}/${data.id}`, data, { method: 'PUT', headers: this.headers })
  }

  async put(data: AnyObj, config={}): Promise<any> {
    return await this.req(`${this.name}`, data, { method: 'PUT' })
  }

  async delete(data: AnyObj): Promise<any> {
    return await this.req(`${this.name}`, data, { method: 'DELETE' })
  }

}

export default BaseApi